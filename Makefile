.PHONY: all get-deps compile run help test

all: compile

get-deps:
	@rebar3 get-deps

compile:
	@rebar3 compile

run:
	@rebar3 shell

test:
	@rebar3 ct

help:
	@echo 'Makefile for my test app                                               '
	@echo '                                                                       '
	@echo 'Usage:                                                                 '
	@echo '   make help                        displays this help text            '
	@echo '   make get-deps                    updates all dependencies           '
	@echo '   make run                         run application in dev mod         '
	@echo '   make compile                     compiles dependencies              '
	@echo '   make test                        runs common tests                  '
	@echo '   make all                         compile                            '
	@echo '                                                                       '
	@echo 'DEFAULT:                                                               '
	@echo '   make all                                                            '
	@echo '                                                                       '

