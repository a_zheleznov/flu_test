flu_test_zhalex
=====

Simple server to get MD5 for inside files

Build
-----

	$ make run
	$ make test

Release manage
-------------
Build release

    rebar3 release
Start for rebar3 v3.9

    ./_build/default/rel/flu_test_rel/bin/flu_test_rel-1.0.1 start
Start for rebar3 v3.20

    ./_build/default/rel/flu_test_rel/bin/flu_test_rel-1.0.1 daemon
Attach to node (^D to exit) 

    ./_build/default/rel/flu_test_rel/bin/flu_test_rel-1.0.1 attach
Stop

    ./_build/default/rel/flu_test_rel/bin/flu_test_rel-1.0.1 stop
Requerements
------------
	Erlang (erlang.org)
	Rebar3


Тест задержек:

	ab -n 100 -c 10 localhost:12080/dev/null
