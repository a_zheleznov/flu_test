-module(read_file_handler).

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_provided/2, content_types_accepted/2]).
-export([handle_request/2]).
-export([read_file_spawner/1]).
-export([read_file/2]).

-define(CHUNK_SIZE, 100*1024*1024).
-define(TIMEOUT, 1*1000).

init(Req, State) ->
    {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"HEAD">>], Req, State}.

content_types_provided(Req, State) ->
    {[{{<<"text">>, <<"html">>, '*'}, handle_request}], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"text">>, <<"html">>, '*'}, handle_request}], Req, State}.

-spec handle_request(Req, State) -> {stop, Req, State}
    when Req :: cowboy_req:req(),
    State :: map().
handle_request(#{method := Method} = Req, State) ->
    io:format("Reqd~p~n,State~p~n", [Req, State]),
    Path = cowboy_req:path(Req),
    io:format("Path~p~n", [Path]),
    Res = case read_file_spawner(Path) of
        {ok, MD5} ->
            case Method of
                <<"GET">> ->
                    cowboy_req:reply(200, #{<<"ETag">> => MD5}, <<"File MD5 loaded">>, Req);
                _ ->
                    cowboy_req:reply(200, #{<<"ETag">> => MD5}, Req)
            end;
        open_error ->
            cowboy_req:reply(400, Req);
        timeout ->
            cowboy_req:reply(503, Req)
    end,
    {stop, Res, State}.

-spec read_file_spawner(Path) -> {Pid, md5, MD5} | open_error | timeout
    when Path :: binary(),
    Pid :: pid(),
    MD5 :: binary().
read_file_spawner(Path) ->
    Pid = erlang:spawn(?MODULE, read_file, [self(), Path]),
    io:format("Pid1: ~p~n", [Pid]),
    Res = receive
        {Pid, md5, MD5} ->
            io:format("T1~n"),
            {ok, MD5};
        {Pid, open_error} ->
            open_error
    after ?TIMEOUT ->
        io:format("T2~n"),
        exit(Pid, kill),
        timeout
    end,
    io:format("Is Alive: ~p~n",[is_process_alive(Pid)]),
    Res.

read_file(Pid, PathToFile) ->
    io:format("Pid2: ~p~n", [Pid]),
    case file:open(PathToFile, [read, binary, raw]) of
        {ok,IoDevice} ->
            MD5_init = erlang:md5_init(),
            MD5_final = read_position(IoDevice, MD5_init, 0),
            file:close(IoDevice),
            MD5 = lists:flatten([io_lib:format("~2.16.0b", [B]) || <<B>> <= MD5_final]),
            io:format("MD5: ~p~n", [MD5]),
            Pid ! {self(), md5, MD5};
        {error, Reason} ->
            io:format("Error file open, Reason: ~p~n", [Reason]),
            Pid ! {self(), open_error}
    end.

read_position(IoDevice, MD5, Offset) ->
    io:format("Offset: ~pMB~n", [round(Offset/1024/1024)]),
    case file:pread(IoDevice, Offset, ?CHUNK_SIZE) of
        {ok, Bin} ->
            MD5_update = erlang:md5_update(MD5, Bin),
            read_position(IoDevice, MD5_update, Offset + ?CHUNK_SIZE);
        eof ->
            erlang:md5_final(MD5)
    end.