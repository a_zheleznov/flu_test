%%%-------------------------------------------------------------------
%% @doc flu_test_zhalex public API
%% @end
%%%-------------------------------------------------------------------

-module(flu_test_zhalex_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([
        {'_', [{'_', read_file_handler, []}]}
    ]),
    {ok, _} = cowboy:start_clear(my_http_listener,
        [{port, 12080}],
        #{env => #{dispatch => Dispatch}}
    ),
    flu_test_zhalex_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
