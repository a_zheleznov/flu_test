-module(load_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-export([
    init_per_testcase/2,
    end_per_testcase/2,
    all/0
]).

-export([
    test_res_200/1,
    test_null_MD5/1,
    test_file_MD5/1,
    test_res_503/1,
    test_res_400/1
]).

-define(APP_NAME, flu_test_zhalex).
-define(NULL_MD5, <<"d41d8cd98f00b204e9800998ecf8427e">>).
-define(TMP_FILE, <<"/tmp/test_MD5">>).
-define(TEST_PHRASE, <<"Test phrase">>).
-define(TEST_PHRASE_MD5, <<"edf60bb55bdee278bbd71f922b7dbd8b">>).

all() -> [
    test_res_200,
    test_null_MD5,
    test_file_MD5,
    test_res_503,
    test_res_400
].

%%dev/random
init_per_testcase(_, Config) ->
    application:ensure_all_started(?APP_NAME),
    application:ensure_all_started(hackney),
    ok = file:write_file(?TMP_FILE, ?TEST_PHRASE),
    Config.

end_per_testcase(_, Config) ->
    ok = file:delete(?TMP_FILE),
    application:stop(flu_test_zhalex),
    Config.

test_res_200(_Config) ->
    {ok, StatusCode, _RespHeaders, _ClientRef} = hackney:request(
        get, <<"localhost:12080/dev/null">>, [], <<>>, []
    ),
    ?assert(200 == StatusCode).

test_null_MD5(_Config) ->
    {ok, _StatusCode, RespHeaders, _ClientRef} = hackney:request(
        get, <<"localhost:12080/dev/null">>, [], <<>>, []
    ),
    ResMD5 = proplists:get_value(<<"ETag">>, RespHeaders),
    ?assert(ResMD5 == ?NULL_MD5).

test_file_MD5(_Config) ->
    {ok, _StatusCode, RespHeaders, _ClientRef} = hackney:request(
        get, <<"localhost:12080", ?TMP_FILE/binary>>, [], <<>>, []
    ),
    ResMD5 = proplists:get_value(<<"ETag">>, RespHeaders),
    ?assert(ResMD5 == ?TEST_PHRASE_MD5).

test_res_503(_Config) ->
    {ok, StatusCode, _RespHeaders, _ClientRef} = hackney:request(get, <<"localhost:12080/dev/zero">>, [], <<>>, []),
    ?assert(503 == StatusCode).

test_res_400(_Config) ->
    {ok, StatusCode, _RespHeaders, _ClientRef} = hackney:request(get, <<"localhost:12080/dev/zero/nothing">>, [], <<>>, []),
    ?assert(400 == StatusCode).
